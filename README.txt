
CS338 Assignment 2 - TripTracker

By Tobias Highfill <trh52@drexel.edu>

#Building and Running

First of all, do this on tux or install Gradle. Once you have Gradle just do:

      gradle run

Optionally through a -q on there to make Gradle shut up. Alternatively I have provided the following make target:

      make run

#Notes

I kinda slapped this together last minute because my laptop is shot. As a result I'm writing this on tux from a library computer and unfortunately I couldn't make it as neat as I'd like. I've tested this on tux and it works so if you have problems locally try that, and if that fails too, please contact me.
