package org.thighfill.triptracker;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JPanel;

import org.thighfill.triptracker.view.DataView;
import org.thighfill.triptracker.view.DataViewPanelBuilder;
import org.thighfill.triptracker.view.DateView;
import org.thighfill.triptracker.view.DoubleView;
import org.thighfill.triptracker.view.TextView;
import org.thighfill.triptracker.view.TimeView;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * Represents a segment of the trip
 */
public abstract class TripSegment {
    public static final String DATE_LABEL = "Date:";
    public static final String TIME_LABEL = "Time:";
    public static final String COMPANY_LABEL = "Company:";
    public static final String PRICE_LABEL = "Price (USD):";
    public static final String CONFIRMATION_LABEL = "Confirmation:";
	
    private Date date = new Date();
    private Date time = new Date();
    private String company = "";
    private double price = 0;
    private String confirmation = "";

    /**
     * Builds the JPanel UI.
     * @param onComplete Runnable to call when Save is clicked and everything is valid
     * @param onCancel Runnable to call when cancel is clicked
     * @returns The JPanel that was built
     */
    public JPanel buildPanel(Runnable onComplete, Runnable onCancel){
	return makeBuilder().buildPanel(onComplete, onCancel);
    }

    /**
     * Creates the builder for the JPanel. Meant to be overridden and extended.
     * @returns The builder to use
     */
    protected DataViewPanelBuilder makeBuilder(){
	DataViewPanelBuilder builder = new DataViewPanelBuilder();
	builder.add(new DateView(DATE_LABEL, date).withOnCompleteAsDate(this::setDate));
	builder.add(new TimeView(TIME_LABEL, time).withOnCompleteAsDate(this::setTime));
	builder.add(new TextView(COMPANY_LABEL, company).withOnCompleteAsString(this::setCompany));
	builder.add(new DoubleView(PRICE_LABEL, price)
		    .withOnCompleteAsNum(this::setPrice).withPrefix("$").withOptional(true));
	builder.add(new TextView(CONFIRMATION_LABEL, confirmation)
		    .withOnCompleteAsString(this::setConfirmation)
		    .withOptional(true));
	return builder;
    }

    /**
     * Uses the builder's toString()
     */
    public String toString(){
	return makeBuilder().toString();
    }
	
    public Date getDate() {
	return date;
    }
    public void setDate(Date date) {
	this.date = date;
    }
    public String getCompany() {
	return company;
    }
    public void setCompany(String company) {
	this.company = company;
    }
    public double getPrice() {
	return price;
    }
    public void setPrice(double price) {
	this.price = price;
    }
    public String getConfirmation() {
	return confirmation;
    }
    public void setConfirmation(String confirmation) {
	this.confirmation = confirmation;
    }

    public Date getTime() {
	return time;
    }

    public void setTime(Date time) {
	this.time = time;
    }
}
