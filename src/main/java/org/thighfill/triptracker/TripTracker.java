package org.thighfill.triptracker;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.stream.Stream;

import javax.swing.*;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This is the main class for the program. It extends {@link JFrame} and adds the stuff we need.
 */
public class TripTracker extends JFrame {
    private static final long serialVersionUID = -1447643834038293850L;

    private JPanel mainPanel = new JPanel();
    private JPanel selectionPanel = new JPanel();
    private JPanel buttonPanel = new JPanel();

    //This is where we store the current editor panel
    private JPanel editorPanel = null;
	
    private JButton addFlightButton = new JButton("Flight", new AddIcon());
    private JButton addTrainButton = new JButton("Train", new AddIcon());
    private JButton addCarButton = new JButton("Car", new AddIcon());
    private JButton removeButton = new JButton(new RemoveIcon());
    private JList<TripSegment> list = new JList<>();
	
    private List<TripSegment> segList = new LinkedList<TripSegment>();
    private TripSegment curr;
	
    public TripTracker() throws HeadlessException {
	super("TripTracker");
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	Stream.of(mainPanel, selectionPanel, buttonPanel, addFlightButton,
		  addTrainButton, addCarButton, removeButton, list)
	    .forEach(c -> c.setVisible(true));
	this.add(mainPanel);
	mainPanel.setLayout(new BorderLayout());
		
	mainPanel.add(selectionPanel, BorderLayout.NORTH);
	selectionPanel.setLayout(new BorderLayout());
	selectionPanel.add(list, BorderLayout.CENTER);
	selectionPanel.add(buttonPanel, BorderLayout.EAST);
		
	buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
	buttonPanel.add(addFlightButton);
	buttonPanel.add(addTrainButton);
	buttonPanel.add(addCarButton);
	buttonPanel.add(removeButton);
		
	list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	list.addListSelectionListener(event -> {
		if(!event.getValueIsAdjusting()){
		    setSegment(list.getSelectedValue());
		}
	    });
		
	addFlightButton.addActionListener(e -> addSegment(new FlightSegment()));
	addTrainButton.addActionListener(e -> addSegment(new TrainSegment()));
	addCarButton.addActionListener(e -> addSegment(new CarSegment()));
	removeButton.addActionListener(e -> removeSegment());
		
	setSegment(null);
    }

    /**
     * Updates the JList to reflect segList
     */
    private void updateList(){
	list.setListData(new Vector<TripSegment>(segList));
    }

    /**
     * Removes the current segment
     */
    private void removeSegment(){
	this.segList.remove(curr);
	updateList();
	curr = null;
    }

    /**
     * Sets the editor panel to reflect the given segment
     * @param seg The Segment to switch to
     * @param onCancel A runnable to execute when cancel is pressed
     */
    private void setSegment(TripSegment seg, Runnable onCancel){
	if(curr == seg) return;
	//System.out.println("Switching to " + seg);
	if(editorPanel != null){
	    mainPanel.remove(editorPanel);
	    editorPanel = null;
	}
	curr = seg;
	removeButton.setEnabled(seg != null);
	if(seg != null){
	    editorPanel = seg.buildPanel(()->repaintComp(list), onCancel);
	    editorPanel.setVisible(true);
	    mainPanel.add(editorPanel, BorderLayout.CENTER);
	}
	//repaintComp(mainPanel);
	// Dirty hack to get the UI to update properly:
	Dimension dim = this.getSize();
	this.pack();
	setSize(dim);
    }

    /**
     * Sets the editor panel to reflect the given segment. Cancel will do nothing
     * @param seg The Segment to switch to
     */
    private void setSegment(TripSegment seg){
	setSegment(seg, ()->{});
    }

    /**
     * Repaints a component
     * @param comp The component to repaint
     */
    private static void repaintComp(JComponent comp){
	comp.repaint(comp.getBounds(null));
    }

    /**
     * Adds a segment to the list and sets it to current. Cancel will remove it.
     * @param seg The segment to add
     */
    private void addSegment(TripSegment seg){
	segList.add(seg);
	updateList();
	list.setSelectedValue(seg, true);
	setSegment(seg, this::removeSegment);
    }
	
    private void start(){
	this.pack();
	this.setSize(600, 800);
	this.setVisible(true);
    }

    public static void main(String[] args) {
	TripTracker tt = new TripTracker();
	tt.start();
    }

}
