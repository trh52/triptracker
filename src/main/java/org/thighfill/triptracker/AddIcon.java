package org.thighfill.triptracker;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.Icon;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This class is a little plus icon for my buttons.
 */
public class AddIcon implements Icon {
	
    private int width = 32, height = 32;

    @Override
    public int getIconHeight() {
	return width;
    }

    @Override
    public int getIconWidth() {
	return height;
    }

    /**
     * Draws the green plus shape
     */
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
	Graphics2D g2d = (Graphics2D) g.create();
		
	int margin = 5;
	g2d.setStroke(new BasicStroke(4));
	g2d.setColor(Color.GREEN);
	g2d.drawLine(x+(width/2), y+margin, x+(width/2), y+height-margin);
	g2d.drawLine(x+margin, y+(height/2), x+width-margin, y+(height/2));
		
	g2d.dispose();
    }

}
