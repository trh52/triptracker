package org.thighfill.triptracker.view;

import java.util.LinkedList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.function.Consumer;

import javax.swing.JComboBox;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This is a class for views that need a dropdown
 */
public class DropdownView<E> extends DataView<JComboBox<E>>{

    private List<E> items;
    private E def; // The default option. Something like "Please choose..."
    private JComboBox<E> comboBox;
    
    @SafeVarargs
    public DropdownView(String label, E curr, E def, E... items) {
	super(label);
	this.def = def;
	this.items = new LinkedList<>(Arrays.asList(items));
	this.items.add(0, def);
	this.comboBox = new JComboBox<>(new Vector<E>(this.items));
	this.comboBox.setVisible(true);
	this.comboBox.setSelectedItem(curr == null ? def : curr);
	this.comboBox.setEditable(false);
    }

    @Override
    public JComboBox<E> getComponent() {
	return comboBox;
    }
    
    @Override
    public void addChangeListener(Consumer<String> consumer){
	comboBox.addItemListener(e -> consumer.accept(getValue()));
    }

    @Override
    public boolean isUnset() {
	return comboBox.getSelectedItem().equals(def);
    }
	
    public DropdownView<E> withOnCompleteAsItem(Consumer<E> consumer){
	this.setOnCompleteAsItem(consumer);
	return this;
    }
	
    @SuppressWarnings("unchecked")
    public void setOnCompleteAsItem(Consumer<E> consumer){
	super.setOnComplete(comp -> consumer.accept((E) comp.getSelectedItem()));
    }

    @Override
    public String getValue() {
	return comboBox.getSelectedItem().toString();
    }

}
