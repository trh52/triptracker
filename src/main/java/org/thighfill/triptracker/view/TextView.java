package org.thighfill.triptracker.view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.util.function.Consumer;

import javax.swing.JTextField;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This class handles views for things represented as text using a {@link JTextField}
 */
public class TextView extends DataView<JTextField> {

    private JTextField textField;
	
    public TextView(String label, String text) {
	super(label);
	textField = new JTextField(text);
	textField.setVisible(true);
    }

    /**
     * Calls the consumer with the current getValue() on each keystroke.
     * Unfortunately it's about one character behind the actual value because this happens before
     * the text is updated
     * @param consumer The consumer to call
     */
    @Override
    public void addChangeListener(Consumer<String> consumer){
	textField.addKeyListener(new KeyAdapter(){
		@Override
		public void keyTyped(KeyEvent e){
		    consumer.accept(getValue());
		}
	    });
    }

    @Override
    public JTextField getComponent() {
	return textField;
    }

    @Override
    public boolean isUnset() {
	return textField.getText().isEmpty();
    }

    // For chaining
    public TextView withOnCompleteAsString(Consumer<String> consumer){
	this.setOnCompleteAsString(consumer);
	return this;
    }

    /**
     * Wraps the onComplete consumer to extract the string
     * @param consumer The consumer to call
     */
    public void setOnCompleteAsString(Consumer<String> consumer){
	super.setOnComplete(comp -> consumer.accept(comp.getText()));
    }

    @Override
    public String getValue() {
	return textField.getText();
    }

}
