package org.thighfill.triptracker.view;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This class is for views on ints.
 */
public class IntView extends NumView<Integer> {

    public IntView(String label, int val) {
	super(label, val);
    }

    @Override
    protected Integer parse(String s) {
	return Integer.parseInt(s);
    }

}
