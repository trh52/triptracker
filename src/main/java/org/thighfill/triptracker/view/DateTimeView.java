package org.thighfill.triptracker.view;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.function.Consumer;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * Convenience class for handling dates and times
 */
public class DateTimeView extends TextView{

    private SimpleDateFormat format;
	
    public DateTimeView(String label, SimpleDateFormat format, Date date) {
	super(label, format.format(date));
	this.format=format;
	// Validator attempts to parse
	super.setValidator(c ->{
		try{
		    this.format.parse(c.getText());
		    return null;
		}catch(ParseException e){
		    return "Must be in the format: "+this.format.toPattern();
		}
	    });
    }
	
    public DateTimeView withOnCompleteAsDate(Consumer<Date> consumer){
	this.setOnCompleteAsDate(consumer);
	return this;
    }
	
    public void setOnCompleteAsDate(Consumer<Date> consumer){
	super.setOnCompleteAsString(str->{
		try {
		    consumer.accept(format.parse(str));
		} catch (ParseException e) {
		    throw new RuntimeException(e);
		}
	    });
    }

}
