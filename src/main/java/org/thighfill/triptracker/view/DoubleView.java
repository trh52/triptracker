package org.thighfill.triptracker.view;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This is a view for decimal values
 */
public class DoubleView extends NumView<Double> {
	
    public DoubleView(String label, double val) {
	super(label, val);
    }

    @Override
    protected Double parse(String s) {
	return Double.parseDouble(s);
    }

}
