package org.thighfill.triptracker.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This class collects a list of {@link DataView}s and builds a {@link JPanel} for them.
 */
public class DataViewPanelBuilder {

    private List<DataView<? extends JComponent>> views = new LinkedList<>();
	
    public DataViewPanelBuilder(){
    }
	
    public DataViewPanelBuilder(List<DataView<? extends JComponent>> views){
	this.views = views;
    }

    /**
     * Adds a view to the list
     * @param view The view to add
     */
    public void add(DataView<? extends JComponent> view){
	views.add(view);
    }

    /**
     * Inserts the given views just before the view corresponding to the given label.
     * @param label The label of the view you want to insert in front of
     * @param newViews The views to add
     */
    @SafeVarargs
    public final void insertBefore(String label, DataView<? extends JComponent>... newViews){
	views.addAll(indexOfLabel(label), Arrays.asList(newViews));
    }

    public int indexOfLabel(String label){
	int i=0;
	for(DataView<? extends JComponent> view : views){
	    if(view.getLabel().equals(label)){
		return i;
	    }
	    ++i;
	}
	return -1;
    }

    /**
     * Replace the view corresponding to the label with the given view.
     * @param label The label of the view you want  to replace
     * @param view The view to replace it with
     */
    public void replace(String label, DataView<? extends JComponent> view){
	views.set(indexOfLabel(label), view);
    }

    /**
     * Finds a view by its label.
     * @param label The label of the view to find
     * @returns The view or null if it can't be found.
     */
    public DataView<? extends JComponent> findByLabel(String label){
	List<DataView<? extends JComponent>> res = views.stream()
	    .filter(v->v.getLabel().equals(label)).collect(Collectors.toList());
	return res.isEmpty() ? null : res.get(0);
    }

    /**
     * This builds the "summary" string by going through the list and concatenating values
     * @returns The summary string
     */
    public String toString(){
	List<String> tokens = views.stream().map(DataView::getValue).collect(Collectors.toList());
	StringBuilder builder = new StringBuilder();
	boolean sep = false;
	for(String tok : tokens){
	    if(sep){
		builder.append(", ");
	    }else{
		sep = true;
	    }
	    builder.append(tok);
	}
	return builder.toString();
    }

    /**
     * Builds the JPanel UI.
     * @param onComplete Runnable to call when Save is clicked and everything is valid
     * @param onCancel Runnable to call when cancel is clicked
     * @returns The JPanel that was built
     */
    public JPanel buildPanel(Runnable onComplete, Runnable onCancel){
	JPanel result = new JPanel(), middle = new JPanel(), bottom = new JPanel();
	JButton save = new JButton("Save"), cancel = new JButton("Cancel");
	JLabel summary = new JLabel(this.toString());
	Stream.of(result, middle, bottom, save, cancel, summary).forEach(c -> c.setVisible(true));
		
	result.setLayout(new BorderLayout());
	result.add(summary, BorderLayout.NORTH);
	result.add(middle, BorderLayout.CENTER);
	result.add(bottom, BorderLayout.SOUTH);

	Map<DataView<? extends JComponent>, JLabel> errLbls = new HashMap<>();
	middle.setLayout(new GridLayout(views.size(), 3));
	views.stream().forEach(view ->{
		//Update the summary everytime it changes
		view.addChangeListener(s->summary.setText(toString()));
		JLabel label = new JLabel(view.getLabel()), err = new JLabel();
		JComponent comp = view.getComponent();
		errLbls.put(view, err);
		comp.setVisible(true);
		label.setVisible(true);
		err.setVisible(false);
		err.setForeground(Color.RED);
		middle.add(label);
		middle.add(comp);
		middle.add(err);
	    });
		
	bottom.setLayout(new FlowLayout());
	bottom.add(save);
	bottom.add(cancel);
		
	save.addActionListener(e -> {
		summary.setText(toString());
		// We want to go thorugh all the views each time to catch all the errors
		// Therefore we can't use .allMatch() because it exits early.
		// So we use forEach() and store the result in a singleton array (to get around the final thing)
		boolean[] valid = {true};
		views.stream().forEach(d -> {
			String err = d.validate();
			JLabel errLbl = errLbls.get(d);
			boolean opt=d.isOptional(), unset=d.isUnset(), isErr=err!=null;
			errLbl.setVisible(false);
			if(isErr){
			    // Ignore it if the user isn't doing this one
			    if(!(opt && unset)){
				errLbl.setVisible(true);
				errLbl.setText(err);
				valid[0] = false;
			    }
			}else if((!opt) && unset){
			    errLbl.setVisible(true);
			    errLbl.setText("This is a required field!");
			    valid[0] = false;
			}
		    });
		if(valid[0]){
		    // Everything checks out
		    views.stream().forEach(d->d.complete());
		    onComplete.run();
		}
	    });
	cancel.addActionListener(e -> {
		summary.setText(toString());
		onCancel.run();
	    });
	
	return result;
    }

    public List<DataView<? extends JComponent>> getViews() {
	return views;
    }

    public void setViews(List<DataView<? extends JComponent>> views) {
	this.views = views;
    }
	
}
