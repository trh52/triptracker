package org.thighfill.triptracker.view;

import java.util.function.Consumer;
import java.util.function.Function;
import javax.swing.JComponent;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This is the general abstract class for a "view" on certain data. The subclasses will maintain
 * a {@link JComponent} and allow for validation.
 */
public abstract class DataView<E extends JComponent> {
	
    private boolean optional = false;
    private String label;
    private Consumer<E> onComplete = e->{};

    // Validators will return null if the data is correct, otherwise it will return an error message
    // for the user
    private Function<E, String> validator = e->null;
	
    public DataView(String label){
	this.label = label;
    }

    /**
     * Get this view's component
     * @returns The component
     */
    public abstract E getComponent();

    /**
     * Check if the user has set a value
     * @returns true if the user has not set a value, false otherwise
     */
    public abstract boolean isUnset();

    /**
     * Gets the current value as a string
     * @returns The current value
     */
    public abstract String getValue();

    /**
     * Add a consumer that will be called with the new value when it's updated
     * @param consumer The consumer to call
     */
    public abstract void addChangeListener(Consumer<String> consumer);

    public void complete(){
	onComplete.accept(getComponent());
    }
	
    public String validate(){
	return validator.apply(getComponent());
    }
	
    public String getLabel() {
	return label;
    }

    public void setLabel(String label) {
	this.label = label;
    }

    public Consumer<E> getOnComplete() {
	return onComplete;
    }

    public void setOnComplete(Consumer<E> onComplete) {
	this.onComplete = onComplete;
    }

    public Function<E, String> getValidator() {
	return validator;
    }

    public void setValidator(Function<E, String> validator) {
	this.validator = validator;
    }
	
    public DataView<E> withOptional(boolean optional){
	setOptional(optional);
	return this;
    }

    public boolean isOptional() {
	return optional;
    }

    public void setOptional(boolean optional) {
	this.optional = optional;
    }
	
}
