package org.thighfill.triptracker.view;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This is a view for dates using a basic {@link SimpleDateFormat}.
 */
public class DateView extends DateTimeView{

    public DateView(String label, SimpleDateFormat format, Date date) {
	super(label, format, date);
    }
	
    public DateView(String label, Date date) {
	this(label, new SimpleDateFormat("MM/dd/yyyy"), date);
    }
	
}
