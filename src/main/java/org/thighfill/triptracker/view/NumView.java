package org.thighfill.triptracker.view;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This abstract view is for numeric types like {@link Double}.
 */
public abstract class NumView<E> extends TextView {

    private Function<E, String> numValidator = e -> null;
    private String prefix = "";
    
    public NumView(String label, E val) {
	super(label, val.toString());
	// Validator attempts to parse the input then passes it to the secondary validator
	super.setValidator(c ->{
		try{
		    return numValidator.apply(parse(c.getText()));
		}catch(NumberFormatException e){
		    return getError(e);
		}
	    });
    }

    /**
     * This allows the subclass to provide a more specific message
     * @param e The error
     * @returns The message
     */
    protected String getError(NumberFormatException e){
	return "Must be a number";
    }

    /**
     * The subclass defines a parser to convert a string into a number
     * @param s String to parse
     * @returns The corresponding number
     */
    protected abstract E parse(String s);

    public String getValue(){
	return prefix + super.getValue();
    }
    
    // For chaining
    public NumView<E> withOnCompleteAsNum(Consumer<E> consumer){
	this.setOnCompleteAsNum(consumer);
	return this;
    }

    /**
     * Wraps the onComplete consumer to parse the number
     * @param consumer The consumer to call
     */
    public void setOnCompleteAsNum(Consumer<E> consumer){
	super.setOnCompleteAsString(s -> consumer.accept(parse(s)));
    }

    public Function<E, String> getNumValidator() {
	return numValidator;
    }

    public void setNumValidator(Function<E, String> numValidator) {
	this.numValidator = numValidator;
    }

    public NumView<E> withPrefix(String prefix){
	setPrefix(prefix);
	return this;
    }
    
    public String getPrefix(){
	return prefix;
    }

    public void setPrefix(String prefix){
	this.prefix = prefix;
    }

}
