package org.thighfill.triptracker.view;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This class handles a view for times using a basic {@link SimpleDateFormat}
 */
public class TimeView extends DateTimeView {

    public TimeView(String label, Date date) {
	super(label, new SimpleDateFormat("hh:mm:ss a"), date);
    }

}
