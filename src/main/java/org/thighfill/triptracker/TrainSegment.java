package org.thighfill.triptracker;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * Represents train ride segments.
 * Currently a stub because {@link TransitSegment} covers everything.
 */
public class TrainSegment extends TransitSegment {

}
