package org.thighfill.triptracker;

import org.thighfill.triptracker.view.DataViewPanelBuilder;
import org.thighfill.triptracker.view.TextView;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * Represents a rental car segment.
 */
public class CarSegment extends TripSegment {

    private String city = "";
    private String location = "";

    protected DataViewPanelBuilder makeBuilder(){
	DataViewPanelBuilder builder = super.makeBuilder();
	// Change the time label to say pick up time
	builder.findByLabel(TripSegment.TIME_LABEL).setLabel("Pick-up time:");
	// Add the new data fields
	builder.insertBefore(TripSegment.PRICE_LABEL,
			     new TextView("City:", city).withOnCompleteAsString(this::setCity),
			     new TextView("Location:", location).withOnCompleteAsString(this::setLocation));
	return builder;
    }
    
    public String getCity(){
	return city;
    }
    public void setCity(String city){
	this.city = city;
    }

    public String getLocation(){
	return location;
    }
    public void setLocation(String location){
	this.location = location;
    }
    
}
