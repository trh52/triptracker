package org.thighfill.triptracker;

import java.util.Arrays;

import org.thighfill.triptracker.view.DataViewPanelBuilder;
import org.thighfill.triptracker.view.DropdownView;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * Represents a flight segment.
 */
public class FlightSegment extends TransitSegment {
	
    private static final String[] AIRLINES = initAirlines();

    @Override
    protected DataViewPanelBuilder makeBuilder() {
	DataViewPanelBuilder builder = super.makeBuilder();
	// Need to replace the company field with an airline field with a dropdown
	builder.replace(TripSegment.COMPANY_LABEL,
			new DropdownView<String>("Airline:", getCompany(), "Select one...", AIRLINES)
			.withOnCompleteAsItem(this::setCompany));
	return builder;
    }
	
    private static String[] initAirlines() {
	String[] airlines={
	    "American Airlines",
	    "United",
	    "JetBlue",
	    "Lufthansa",
	    "British Airways",
	    "Air France",
	    "Icelandair",
	    "Air Canada",
	    "Ryan Air",
	};
	Arrays.sort(airlines);
	return airlines;
    }
}
