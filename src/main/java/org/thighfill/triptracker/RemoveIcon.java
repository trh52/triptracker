package org.thighfill.triptracker;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.Icon;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * Represents a little red X shape for my button.
 */
public class RemoveIcon implements Icon{
	
    private int width = 32, height = 32;

    @Override
    public int getIconHeight() {
	return width;
    }

    @Override
    public int getIconWidth() {
	return height;
    }

    
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
	Graphics2D g2d = (Graphics2D) g.create();

	// Paint the red X
	int margin = 5;
	g2d.setStroke(new BasicStroke(4));
	g2d.setColor(Color.RED);
	g2d.drawLine(x+margin, y+margin, x+width-margin, y+height-margin);
	g2d.drawLine(x+margin, y+height-margin, x+width-margin, y+margin);
		
	g2d.dispose();
    }

}
