package org.thighfill.triptracker;

import java.util.Date;

import org.thighfill.triptracker.view.DataViewPanelBuilder;
import org.thighfill.triptracker.view.IntView;
import org.thighfill.triptracker.view.TextView;
import org.thighfill.triptracker.view.TimeView;

/**
 * Tobias Highfill
 * trh52@drexel.edu
 * CS338:GUI, Assignment 2, Program 1
 *
 * This is a class to represent trip segments that involve travel such as flights and train rides.
 */
public class TransitSegment extends TripSegment {
	
    private String departureCity = "", arrivalCity = "";
    private Date arrivalTime = new Date();
    private int number = 0;

    @Override
    protected DataViewPanelBuilder makeBuilder() {
	DataViewPanelBuilder builder = super.makeBuilder();
	// Add the new data fields before the price
	builder.insertBefore(TripSegment.PRICE_LABEL,
			     new TextView("Departing from:", departureCity).withOnCompleteAsString(this::setDepartureCity),
			     new TextView("Arriving in:", arrivalCity).withOnCompleteAsString(this::setArrivalCity),
			     new TimeView("Arriving at:", arrivalTime).withOnCompleteAsDate(this::setArrivalTime),
			     new IntView("Number:", number).withOnCompleteAsNum(this::setNumber));
	return builder;
    }

    public String getDepartureCity() {
	return departureCity;
    }

    public void setDepartureCity(String departureCity) {
	this.departureCity = departureCity;
    }

    public String getArrivalCity() {
	return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
	this.arrivalCity = arrivalCity;
    }

    public Date getArrivalTime() {
	return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
	this.arrivalTime = arrivalTime;
    }

    public int getNumber() {
	return number;
    }

    public void setNumber(int number) {
	this.number = number;
    }

}
